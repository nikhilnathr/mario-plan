const initState  = {
  projects: [
      { id: '1', title: 'Get muffins', content: 'This is the content for this project. This might be long.' },
      { id: '2', title: 'Kill all thugs', content: 'This is the content for this project. This might be long.' },
      { id: '3', title: 'Buy more ammo', content: 'This is the content for this project. This might be long.' },
    ]
};

const projectReducer = (state = initState, action) =>  {
  switch (action.type) {
    case 'CREATE_PROJECT':
      console.log("created project", action.project);
      break;
    default:
      break;
  }
  return state;
}

export default projectReducer;
