import React from 'react';

const ProjectDetails = (props) => {
  const id = props.match.params.id;
  return (
    <div className="container section project-details">
      <div className="card z-depth-0">
        <span className="card-title">Project Title - { id }</span>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est placeat molestias maiores fuga doloremque, ea nemo tempore commodi obcaecati quisquam rem ipsam, iste vitae. Obcaecati nihil voluptates, adipisci reiciendis odit.</p>
        <div className="card-action grey lighten-4 grey-text">
          <div>Posted by Nikhil Nath</div>
          <div>27th February, 2pm</div>
        </div>
      </div>
    </div>
  );
}

export default ProjectDetails;
