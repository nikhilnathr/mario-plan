import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCLymsPciKfmMdkGVFDHCKxfqGRWCvYDVM",
  authDomain: "marioplan-dreamhatx.firebaseapp.com",
  databaseURL: "https://marioplan-dreamhatx.firebaseio.com",
  projectId: "marioplan-dreamhatx",
  storageBucket: "marioplan-dreamhatx.appspot.com",
  messagingSenderId: "301307141338"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
